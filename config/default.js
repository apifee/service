const path = require('path');
const _    = require('lodash');

const ROOT = path.join(__dirname, '..');

module.exports = {
  authorizationEnabled: true,
  jwtSecret: 'ethbuenosaires',

  apis: {
    v1: {
      baseUri:     '/api/v1',
      raml:        path.join(ROOT, '/assets/raml/api.v1.raml'),
      jsonMaxSize: '400kb'
    }
  },
  express: {
    host: '0.0.0.0',
    port: _.get(process.env, 'PORT', 8080)
  },
  knex: {
    debug:      false,
    client:     'pg',
    connection: {
      host:     _.get(process.env, 'DATABASE_HOSTNAME', '127.0.0.1'),
      user:     _.get(process.env, 'DATABASE_USERNAME', 'postgres'),
      password: _.get(process.env, 'DATABASE_PASSWORD', ''),
      database: _.get(process.env, 'DATABASE_NAME',     'Apifee'),
      charset:  'utf8'
    }
  },
  logger: {
    console: {
      enabled:     true,
      level:       'info',
      timestamp:   true,
      prettyPrint: true
    },
    syslog: {
      enabled:  false,
      protocol: 'udp4',
      path:     '/dev/log',
      app_name: 'microservice-template',
      facility: 'local6'
    }
  },
  services: {
    cs: {
      uri: {
        protocol: _.get(process.env, 'CORE_SERVICES_PROTOCOL', 'https'),
        hostname: _.get(process.env, 'CORE_SERVICES_HOST', 'qax.anypoint.mulesoft.com'),
        port:     _.parseInt(_.get(process.env, 'CORE_SERVICES_PORT', 443)),
        pathname: _.get(process.env, 'CORE_SERVICES_BASE_PATH', 'accounts')
      }
    }
  }
};
