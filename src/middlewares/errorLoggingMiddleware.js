module.exports = function errorLoggingMiddleware(logger) {
  // eslint-disable-next-line no-shadow
  return function errorLoggingMiddleware(err, req, res, next) {
    logger.error(err);
    next(err);
  };
};