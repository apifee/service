const jwt = require('jsonwebtoken');

module.exports = function authMiddleware(config) {
  return function innerAuthMiddleware(req, res, next) {
    let userId;

    try {
      userId = config.authorizationEnabled ?
      jwt.verify(req.headers['authorization'], config.jwtSecret).userId :
      req.headers['x-user-id'];
    }
    catch (err) {
      return res.status(401).json({
        status: 401,
        message: "Not authenticated"
      });
    }

    req.context.userId = userId;
    return next();
  };
};
