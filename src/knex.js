/* (c) 2003-2017 MuleSoft, Inc. This software is protected under international
 * copyright law. All use of this software is subject to MuleSoft's Master
 * Subscription Agreement (or other Terms of Service) separately entered
 * into between you and MuleSoft. If such an agreement is not in
 * place, you may not use the software.
 **/

const Knex = require('knex');

module.exports = function knex(
  config,
  logger
) {

  // eslint-disable-next-line new-cap
  const knexInstance = Knex(config.knex);

  checkConnection();

  return knexInstance;

  function checkConnection() {

    knexInstance.raw('select 1+1 as result').then(() => {
      logger.info('Connection to the database established successfully');
    }).catch((err) => {
      logger.info('Connection to the database was not established: ' + err);
    });
  }
};
