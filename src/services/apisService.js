const bluebird = require('bluebird');
const uuid     = require('uuid/v4');
const _        = require('lodash');
const web3     = require('web3');

module.exports = function apisService(
    config,
    knex,
    userService
  ) {
  return {
    addTerms,
    create,
    getAll,
    getAllByOwnerId,
    getById
  };

  function assignTermsIDandHash(term) {
    const hashedTerms = web3.utils.sha3(JSON.stringify({
      minCredits: term.minCredits,
      maxCredits: term.maxCredits,
      valueCreditsInWei: term.valueCreditsInWei
    }));

    return _.assign({}, term, {
      id: uuid(),
      termsHash: hashedTerms
    });
  }

  function addTerms(term, apiId) {
    return getById(apiId)
      .then(function (api) {
        api.terms.push(assignTermsIDandHash(term));
        return knex
          .update({terms: JSON.stringify(api.terms)})
          .from('apis')
          .where('id', apiId);
      })
      .then( result => getById(apiId));
  }

  /**
   * Get
   *
   * @returns {Promise}
   */
  function getAll() {
    return knex.select().from('apis').then(enrichAPIs);
  }

  function getById(apiId) {
    return knex.select()
      .from('apis')
      .where('id', apiId)
      .first()
      .then(enrichAPI);
  }

  function create(api, userId) {
    api.terms = api.terms.map(assignTermsIDandHash);

    return knex.insert({
      id: uuid(),
      name: api.name,
      description: api.description,
      url: api.url,
      providerAddress: api.providerAddress,
      ownerId: userId,
      // json datatype has an issue with json arrays.
      terms: JSON.stringify(api.terms)
    },'id').into('apis').then(function(rows) {
      return getById(rows[0]);
    });
  }

  function getAllByOwnerId(userId) {
    return knex.select()
      .from('apis')
      .where('ownerId', userId)
      .then(enrichAPIs)
  }

  function enrichAPIs(apis) {
    return bluebird.map(apis, enrichAPI);
  }

  function enrichAPI(api) {
    return userService.getById(api.ownerId)
      .then(function (user) {
        api.ownerName = user.name;
        api.terms     = JSON.parse(api.terms);
        return api;
      });
  }
};
