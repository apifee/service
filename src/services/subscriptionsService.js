const bluebird    = require('bluebird');
const uuid        = require('uuid/v4');
const _           = require('lodash');
const createError = require('http-errors');

module.exports = function subscriptionsService(
    apisService,
    chargesService,
    config,
    knex,
    superagent
  ) {
  return {
    create,
    getByApiId,
    getById,
    getAllByOwnerId,
    patch
  };

  function getByApiId(apiId) {
    return knex.select()
      .from('subscriptions')
      .where('apiId', apiId)
      .then(enrichSubscriptions);
  }

  function getById(subscriptionId) {
    return knex.select()
      .from('subscriptions')
      .where('id', subscriptionId)
      .first()
      .then(enrichSubscription);
  }

  function getAllByOwnerId(userId) {
    return knex.select()
      .from('subscriptions')
      .where('ownerId', userId)
      .then(enrichSubscriptions)
  }

  function patch(subscription, subscriptionId) {
    // delete subscription.termId;
    delete subscription.id;

    return knex
      .update(subscription)
      .from ('subscriptions')
      .where('id', subscriptionId)
      .then(updatedSubscription => getById(subscriptionId));

    /*
     * TODO if a contract address is provided, APIFee should
     * validate with the contract and if everything is ok,
     * change the status to confirmed/approved
     */
  }

  function create(subscription, userId) {
    if (subscription.charges.length != 1) {
      return Promise.reject(createError(400, 'One charge(and only one) needs to be registered at creation'));
    }

    return apisService.getById(subscription.apiId)
      .tap(function (api) {
        subscription.charges.forEach(function (charge) {
          if(!_.some(api.terms, term => term.id === charge.termsId)){
            return Promise.reject(createError(400, 'Please enter charges with valid terms ID'));
          }
        });
      })
      .then(function (api) {
        return knex.insert({
          id: uuid(),
          apiId: subscription.apiId,
          contractAddress: subscription.contractAddress,
          signingAddress: subscription.signingAddress,
          ownerId: userId,
        },'id')
        .into('subscriptions')
        .tap(function(subscriptionRows) {
          return chargesService.create(subscriptionRows[0],subscription.charges[0].termsId)
        })
        .then(function(subscriptionRows) {
          return getById(subscriptionRows[0]);
        })
      });
  }

  function enrichSubscriptions(subscriptions) {
    return bluebird.map(subscriptions, enrichSubscription);
  }

  function enrichSubscription(subscription) {
    let api;

    return apisService.getById(subscription.apiId)
      .then(function (apiFound) {
        // Will need API to enrich terms info.
        // subscription.terms = _.find(api.terms, term => term.id === subscription.termsId);
        subscription.api = apiFound;
        return chargesService.getBySubscriptionId(subscription.id);
      })
      .then(function (charges) {
        subscription.charges = charges;
        return retrieveCreditsInformation(subscription);
      })
      .then(function (creditsInformation){
        subscription.totalCredits           = creditsInformation.credits;
        subscription.consumedCredits        = creditsInformation.consumedCredits;
        subscription.creditsPendingWithdraw = creditsInformation.creditsPendingWithdraw;
        subscription.signature              = creditsInformation.signature;
        console.log(subscription);
        return subscription;
      });
  }

  function retrieveCreditsInformation(subscription) {
    if (!subscription.contractAddress) {
      return {
        credits: 0,
        consumedCredits: 0,
        creditsPendingWithdraw: 0
      };
    }

    return superagent('GET', subscription.api.url + '/info/' + subscription.contractAddress)
      .then(body)
      // .catch((err) => {
      //   console.log('Error while getting info from gateway');
      //   return {};
      // });
  }

  function body(res) {
    return bluebird.resolve(res.ok ? res.body : {});
  }
};
