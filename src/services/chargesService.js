const bluebird = require('bluebird');
const uuid        = require('uuid/v4');

module.exports = function chargesService(
    config,
    knex
  ) {
  return {
    create,
    patch,
    getById,
    getBySubscriptionId
  };

  function create(subscriptionId, termsId){
    return knex.insert({
      id: uuid(),
      status: 'PENDING',
      termsId: termsId,
      subscriptionId: subscriptionId,
    },'id')
    .into('charges')
    .then(chargesRows => getById(chargesRows[0]));
  }

  function getById(chargeId) {
    return knex.select()
      .from('charges')
      .where('id', chargeId)
      .first()
      .then(enrichCharge);
  }

  function getBySubscriptionId(subscriptionId){
    return knex.select()
      .from('charges')
      .where('subscriptionId', subscriptionId)
      .then(enrichCharges)
  }

  function enrichCharges(charges) {
    return bluebird.map(charges, enrichCharge);
  }

  function enrichCharge(charge) {
    return charge;
  }

  function patch(charge, chargeId) {
    delete charge.id;

    return knex
      .update(charge)
      .from ('charges')
      .where('id', chargeId)
      .then(updatedCharge => getById(chargeId));
  }
};
