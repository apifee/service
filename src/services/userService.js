const bluebird = require('bluebird');

module.exports = function userService(
    config,
    knex
  ) {
  return {
    getById,
    getByUserAndPassword
  };


  function getById(userId) {
    return knex.select()
      .from('users')
      .where('id', userId)
      .first();
  }

  function getByUserAndPassword(username, password) {
    return knex.select()
      .from('users')
      .where({username: username, password: password})
      .first();
  }
};
