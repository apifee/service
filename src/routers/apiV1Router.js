const cors = require('cors');
const express = require('express');
const ospreyMiddleware = require('osprey-middleware');

module.exports = function apiV1Router(
  authMiddleware,
  config,
  apisController,
  loginController,
  statusController,
  subscriptionsController
) {
  return express.Router().use(config.apis.v1.baseUri, express.Router()

    /**
     * OSPREY
     */
    // .use(ospreyMiddleware(config.apis.v1.raml, {
    //   disableErrorInterception: true,
    //   server:                   {
    //     limit: config.apis.v1.jsonMaxSize
    //   }
    // }))
     .use(cors())
     .use(initContext)

    // Ping
     .get('/ping', statusController.ping)

    // Health
     .get('/health', statusController.health)

    /**
     * GENERAL
     */


    /**
     * ROUTES
     */

    .post('/login', loginController.login)

    .use(authMiddleware)

    // APIS
    .get ('/apis', apisController.getAll)
    .post('/apis', apisController.create)
    .get ('/apis/:apiId',apisController.getById)
    .get ('/apis/:apiId/subscriptions',subscriptionsController.getByApiId)
    .post('/apis/:apiId/terms', apisController.addTerms)
    .get ('/me/apis', apisController.getAllByOwnerId)

    // Subscriptions
    .get  ('/me/subscriptions', subscriptionsController.getAllByOwnerId)
    .post ('/me/subscriptions', subscriptionsController.create)
    .get  ('/me/subscriptions/:subscriptionId', subscriptionsController.getById)
    .patch('/me/subscriptions/:subscriptionId', subscriptionsController.patch)

    // Charges
    .post ('/me/subscriptions/:subscriptionId/charges',           subscriptionsController.addCharge)
    .get  ('/me/subscriptions/:subscriptionId/charges/:chargeId', subscriptionsController.getChargeById)
    .patch('/me/subscriptions/:subscriptionId/charges/:chargeId', subscriptionsController.patchCharge)
  );
};

function initContext(req, res, next) {
  req.context = {};
  next();
}
