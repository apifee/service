const express = require('express');
const bodyParser = require('body-parser');

module.exports = function $app(
  apiV1Router,
  errorMiddleware,
  errorLoggingMiddleware
) {
  const app = express();

  app.use(bodyParser.json());

  app.use(apiV1Router);

  app.use(errorLoggingMiddleware);
  app.use(errorMiddleware);

  return app;
};
