const expressify = require('expressify');

// TODO: This module provides an example of a controller. Replace it with your actual controller/s
module.exports = function subscriptionsController(
  chargesService,
  subscriptionsService
) {
  return expressify({
    addCharge,
    create,
    getByApiId,
    getById,
    getChargeById,
    getAllByOwnerId,
    patch,
    patchCharge
  });

  function getById(req, res) {
    return subscriptionsService.getById(req.params.subscriptionId)
      .then((response) => res.json(response));
  }

  function getByApiId(req, res) {
    return subscriptionsService.getByApiId(req.params.apiId)
      .then((response) => res.json(response));
  }

  function getAllByOwnerId(req, res) {
    const userId = req.context.userId;
    return subscriptionsService.getAllByOwnerId(userId)
      .then((response) => res.json(response));
  }

  function patch(req, res) {
    return subscriptionsService.patch(req.body, req.params.subscriptionId)
      .then((response) => res.json(response));
  }

  function create(req, res) {
    const userId = req.context.userId;
    return subscriptionsService.create(req.body, userId)
      .then((response) => res.status(201).json(response));
  }

  function addCharge(req, res) {
    return chargesService.create(req.params.subscriptionId, req.body.termsId)
      .then((response) => res.status(201).json(response));
  }

  function getChargeById(req, res) {
    return chargesService.getById(req.params.chargeId)
      .then((response) => res.status(200).json(response));
  }

  function patchCharge(req, res) {
    return chargesService.patch(req.body, req.params.chargeId)
      .then((response) => res.status(200).json(response));
  }


};
