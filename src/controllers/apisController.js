const expressify = require('expressify');

// TODO: This module provides an example of a controller. Replace it with your actual controller/s
module.exports = function apisController(
  apisService
) {
  return expressify({
    addTerms,
    create,
    getAll,
    getAllByOwnerId,
    getById
  });

  /**
   * It performs a get.
   *
   * @returns {Promise}
   */
  function getAll(req, res) {
    return apisService.getAll()
      .then((response) => res.json(response));
  }

  function getById(req, res) {
    return apisService.getById(req.params.apiId)
      .then((response) => res.json(response));
  }

  function getAllByOwnerId(req, res) {
    const userId = req.context.userId;
    return apisService.getAllByOwnerId(userId)
      .then((response) => res.json(response));
  }

  function create(req, res) {
    const userId = req.context.userId;
    return apisService.create(req.body, userId)
      .then((response) => res.status(201).json(response));
  }

  function addTerms(req, res) {
    return apisService.addTerms(req.body, req.params.apiId)
      .then((response) => res.status(201).json(response));
  }
};
