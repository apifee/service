const expressify = require('expressify');
const jwt        = require('jsonwebtoken');

module.exports = function loginController(config, userService) {
  return expressify({
    login
  });

  function login(req, res) {
    return userService
      .getByUserAndPassword(req.body.username, req.body.password)
      .then(function (foundUser) {
        if(!foundUser)
          res.status(401).json({
            status:  401,
            message: 'Not a valid user'
          });

        const signedToken = jwt.sign({userId: foundUser.id}, config.jwtSecret);
        res.status(201).json({ token: signedToken });
      })
  }
};
