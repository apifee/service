'use strict';

const multiline = require('multiline');

exports.up = function(knex, Promise) {
  return knex.raw(multiline(function () {/*
    CREATE TABLE "apis"
    (
	  	"id" varchar(40) PRIMARY KEY,
  		"name" varchar(100) NOT NULL,
  		"description" varchar(1000) NOT NULL,
  		"url" varchar(100) NOT NULL,
  		"ownerId" varchar(40) NOT NULL,
  		"terms"	text NULL,
      "providerAddress" varchar(200) NOT NULL
    );
  */}));
};

exports.down = function(knex, Promise) {
 return knex.raw(multiline(function () {/*
    DROP TABLE "apis"

  */}));
};
