
exports.up = function(knex, Promise) {
  return knex.raw(`
    insert into apis(id, name, description, url, "ownerId", terms, "providerAddress") values ('57cf7ba8-61aa-11e8-879d-abbde4afcc3d', 'Weather API', 'Our weather API is simple, clear and free. We also offer higher levels of support, please see our paid plan options. To access the API you need to sign up for an API key if you are on a free or paid plan.', 'https://www.openweathermap.org/api', 'fe6c8c25-c70b-4986-be8f-23c2cbc604d1', '[{"valueCreditsInWei":"2000000000000","minCredits":"10","maxCredits":"1000000000","id":"26d884c4-969b-43c7-8d4c-3f157cf6ecc9","termsHash":"0x45fcc724672dd68624f655b61392e0a1536f88dea2a3eb60b7fc22173e810c5e"}]', '0x0');

    insert into apis(id, name, description, url, "ownerId", terms, "providerAddress") values ('3dd22c36-61ab-11e8-95fa-63af65d1792f', 'JSONPlaceholder', 'JSONPlaceholder is a free online REST service that you can use whenever you need some fake data. It is great for tutorials, faking a server, sharing code examples', 'https://jsonplaceholder.typicode.com/', 'fe6c8c25-c70b-4986-be8f-23c2cbc604d1', '[{"valueCreditsInWei":"2000000000000","minCredits":"10","maxCredits":"1000000000","id":"26d884c4-969b-43c7-8d4c-3f157cf6ecc9","termsHash":"0x45fcc724672dd68624f655b61392e0a1536f88dea2a3eb60b7fc22173e810c5e"}]', '0x0');
  `);
};

exports.down = function(knex, Promise) {

};
