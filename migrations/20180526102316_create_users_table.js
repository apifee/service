'use strict';

const multiline = require('multiline');

exports.up = function(knex, Promise) {
  return knex.raw(multiline(function () {/*
    CREATE TABLE "users"
    (
      "id" varchar(40) PRIMARY KEY,
  		"name" varchar(600) NOT NULL,
  		"username" varchar(40) NOT NULL,
  		"password" varchar(100) NOT NULL
    );

	insert into users(id,name,username,password) values('fe6c8c25-c70b-4986-be8f-23c2cbc604d1','Mariano Campo', 'mariano', 'campo');
	insert into users(id,name,username,password) values('fe6c8c25-c70b-4986-be8f-23c2cbc604d2','Nicolas Mouso', 'nicolas', 'mouso');
	insert into users(id,name,username,password) values('fe6c8c25-c70b-4986-be8f-23c2cbc604d3','Tomas Bruno', 'tomas', 'bruno');

  */}));
};

exports.down = function(knex, Promise) {
 return knex.raw(multiline(function () {/*
    DROP TABLE "users"

  */}));
};
