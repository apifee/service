'use strict';

const multiline = require('multiline');

exports.up = function(knex, Promise) {
  return knex.raw(multiline(function () {/*
    CREATE TABLE "subscriptions"
    (
      "id" varchar(40) PRIMARY KEY,
  		"apiId" varchar(40) NOT NULL,
  		"ownerId" varchar(40) NOT NULL,
  		"contractAddress" varchar(100) NULL,
      "signingAddress" varchar(200) NOT NULL
    );

  */}));
};

exports.down = function(knex, Promise) {
 return knex.raw(multiline(function () {/*
    DROP TABLE "subscriptions"

  */}));
};
