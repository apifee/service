'use strict';

const multiline = require('multiline');

exports.up = function(knex, Promise) {
  return knex.raw(multiline(function () {/*
    ALTER TABLE "apis"
    	ADD CONSTRAINT fk_apis_owners
    	FOREIGN KEY ("ownerId")
    	references users("id");

 	ALTER TABLE "subscriptions"
	    ADD CONSTRAINT fk_subscriptions_owners
	    FOREIGN KEY ("ownerId") references users("id");

    ALTER TABLE "subscriptions"
    	ADD CONSTRAINT fk_subscriptions_apis
    	FOREIGN KEY ("apiId") references apis("id");

  */}));
};

exports.down = function(knex, Promise) {
 return knex.raw(multiline(function () {/*
	ALTER TABLE "apis" 			drop CONSTRAINT fk_apis_owners;
	ALTER TABLE "subscriptions" drop CONSTRAINT fk_subscriptions_owners;
	ALTER TABLE "subscriptions" drop CONSTRAINT fk_subscriptions_apis;

  */}));
};