'use strict';

const multiline = require('multiline');

exports.up = function(knex, Promise) {
  return knex.raw(multiline(function () {/*
    CREATE TABLE "charges"
    (
      "id" varchar(40) PRIMARY KEY,
  	  "status" varchar(40) NOT NULL,
  	  "subscriptionId" varchar(40) NOT NULL,
  	  "termsId" varchar(40) NOT NULL
    );

  */}));
};

exports.down = function(knex, Promise) {
 return knex.raw(multiline(function () {/*
    DROP TABLE "charges"

  */}));
};
